module.exports = function (app) {
  if (!app) throw new Error('Missing parameter: "app" not provided.');

  const express = require('express'),
    EventRouter = express.Router(),
    EventProvider = require('./EventProvider')(app),
    VerifyToken = require(__root + 'helpers/VerifyToken')(app);

  // ADD EVENT
  EventRouter.post('/add', VerifyToken, EventProvider.add);

  // UPDATE EVENT
  EventRouter.post('/update', VerifyToken, EventProvider.update);

  // GET EVENTS
  EventRouter.post('/getByUserId/:id', VerifyToken, EventProvider.getByUserId);

  return EventRouter;
};
