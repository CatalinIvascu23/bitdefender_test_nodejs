/* eslint-disable no-console */
const Event = require('./Event');
const moment = require('moment');

module.exports = function () {
  return {
    add,
    update,
    getByUserId,
  };

  function validateEvent(eventBody, res) {
    const { event, startDate, endDate, recurrence, eventDuration, userId } =
      eventBody;

    if (!event) {
      return res.status(400).send('Event is required.');
    }

    if (!startDate) {
      return res.status(400).send('Start date is required.');
    }

    if (!endDate) {
      return res.status(400).send('End date is required.');
    } else if (moment(endDate).diff(moment(startDate)) < 0)
      return res.status(400).send('End date is less than start date');

    if (!recurrence) {
      return res.status(400).send('Recurrence is required.');
    }

    if (!eventDuration) {
      return res.status(400).send('Event duration is required.');
    }

    if (!userId) {
      return res.status(400).send('Event userId is required.');
    }
  }

  function add(req, res, next) {
    const event = req.body;
    validateEvent(event, res);
    // event.user = req.id;

    Event.create(event)
      .then((response) => {
        res.status(201).send(response);
      })
      .catch((err) => next(err));
  }

  function update(req, res, next) {
    const { event } = req.body;

    validateEvent(event, res);

    event.user.id = req.id;

    Event.findByIdAndUpdate(req.params.id, event, { new: true })
      .then((updatedEvent) => {
        if (updatedEvent) {
          res.status(200).send(updatedEvent);
        }
      })
      .catch((err) => next(err));
  }

  function getByUserId(req, res, next) {
    const id = req.params.id;

    Event.find({ userId: id })
      .then((event) => {
        if (event) {
          return res.status(200).send(event);
        }
        return res.status(400).send('No event found.');
      })
      .catch((err) => {
        return next(err);
      });
  }
};
