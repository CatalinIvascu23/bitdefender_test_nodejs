const mongoose = require('mongoose');
const EventSchema = new mongoose.Schema({
  event: {
    type: String,
    trim: true,
  },
  startDate: String,
  endDate: String,
  recurrence: String,
  eventDuration: String,
  userId: String,
});
const Event = mongoose.model('Event', EventSchema);
module.exports = Event;
