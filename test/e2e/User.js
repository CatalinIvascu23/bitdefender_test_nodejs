const expect = require('chai').expect;
const request = require('chai').request;
/*
 * Server setup
 */
const app = mutil.getApp();

const rTestUserWithUserName = {
  password: 'password123',
  confirmPassword: 'password123',
  userName: 'Test Test',
};

const rTestUserWithInvalidUserName = {
  password: 'password123',
  confirmPassword: 'password123',
  userName: 'T',
};

const rTestUserWithUserNameNumber = {
  userName: 12312415,
  password: 'password123',
  confirmPassword: 'password123',
};

const rTestUserWithEmptyPassword = {
  userName: 'Test Test',
  password: '',
  confirmPassword: 'password123',
};

const rTestUserWithEmptyConfirmPassword = {
  userName: 'Test Test',
  password: 'password123',
  confirmPassword: '',
};

const rTestUserWithDifferentPassword = {
  userName: 'Test Test',
  password: 'password123',
  confirmPassword: '123password',
};

const rTestUserWithEmptyUserName = {
  userName: '',
  password: 'password123',
  confirmPassword: 'password123',
};

const rTestUserWithInvalidPassword = {
  password: '11',
  confirmPassword: '11',
  userName: 'Test Test',
};

const rTestUserWithInvalidConfirmPassword = {
  password: 'password123',
  confirmPassword: '123password',
  userName: 'Test Test',
};

const testUserWithoutPassword = {
  userName: 'Test Test',
};

const testUserWithInvalidCredentials = {
  userName: 'T',
  password: 'password',
};

const testUserWithInvalidPassword = {
  userName: 'Test Test',
  password: '3',
};

describe('UserRouter', () => {
  describe('UserProvider', () => {
    describe('.registerUser', () => {
      it('should add a new User to the user collection', (done) => {
        request(app)
          .post('/api/users/register')
          .send(rTestUserWithUserName)
          .then((res) => {
            expect(res).to.have.status(201);
            expect(res.text).to.be.equal('User created');
            done();
          });
      });

      it('should fail to add the same User to the user collection', (done) => {
        request(app)
          .post('/api/users/register')
          .send(rTestUserWithUserName)
          .then((res) => {
            expect(res).to.have.status(400);
            expect(res.text).to.be.equal('User already exists.');
            done();
          });
      });

      it('should not register a user with numeric user name.', (done) => {
        request(app)
          .post('/api/users/register')
          .send(rTestUserWithUserNameNumber)
          .then((res) => {
            expect(res).to.have.status(400);
            expect(res.text).to.be.equal('Invalid user name.');
            done();
          });
      });

      it('should not register a user with empty password.', (done) => {
        request(app)
          .post('/api/users/register')
          .send(rTestUserWithEmptyPassword)
          .then((res) => {
            expect(res).to.have.status(400);
            expect(res.text).to.be.equal('Password is required.');
            done();
          });
      });

      it('should not register a user with empty confirmPassword.', (done) => {
        request(app)
          .post('/api/users/register')
          .send(rTestUserWithEmptyConfirmPassword)
          .then((res) => {
            expect(res).to.have.status(400);
            expect(res.text).to.be.equal('Password is required.');
            done();
          });
      });

      it('should not register a user with different passwords.', (done) => {
        request(app)
          .post('/api/users/register')
          .send(rTestUserWithDifferentPassword)
          .then((res) => {
            expect(res).to.have.status(400);
            expect(res.text).to.be.equal(
              'Password and confirm password must match.'
            );
            done();
          });
      });

      it('should not register a user with empty string user name.', (done) => {
        request(app)
          .post('/api/users/register')
          .send(rTestUserWithEmptyUserName)
          .then((res) => {
            expect(res).to.have.status(400);
            expect(res.text).to.be.equal('User name is required.');
            done();
          });
      });

      it('should not register a user with numeric user name.', (done) => {
        request(app)
          .post('/api/users/register')
          .send(rTestUserWithUserNameNumber)
          .then((res) => {
            expect(res).to.have.status(400);
            expect(res.text).to.be.equal('Invalid user name.');
            done();
          });
      });

      it('should not register a user with invalid user name.', (done) => {
        request(app)
          .post('/api/users/register')
          .send(rTestUserWithInvalidUserName)
          .then((res) => {
            expect(res).to.have.status(400);
            expect(res.text).to.be.equal('Invalid user name.');
            done();
          });
      });

      it('should not register a user with invalid password.', (done) => {
        request(app)
          .post('/api/users/register')
          .send(rTestUserWithInvalidPassword)
          .then((res) => {
            expect(res).to.have.status(400);
            expect(res.text).to.be.equal('Invalid password.');
            done();
          });
      });

      it('should not register a user with invalid confirm password.', (done) => {
        request(app)
          .post('/api/users/register')
          .send(rTestUserWithInvalidConfirmPassword)
          .then((res) => {
            expect(res).to.have.status(400);
            expect(res.text).to.be.equal(
              'Password and confirm password must match.'
            );
            done();
          });
      });
    });

    describe('.loginUser', () => {
      it('should login the user ', (done) => {
        request(app)
          .post('/api/users/login')
          .send(rTestUserWithUserName)
          .then((res) => {
            const body = res.body;
            token = res.header.token;
            expect(res).to.have.status(200);
            done();
          });
      });

      it('should not login a user without user name.', (done) => {
        request(app)
          .post('/api/users/login')
          .send({ password: 'parola124' })
          .then((res) => {
            expect(res).to.have.status(400);
            expect(res.text).to.be.equal('Username is required.');
            done();
          });
      });

      it('should not login a user without password.', (done) => {
        request(app)
          .post('/api/users/login')
          .send(testUserWithoutPassword)
          .then((res) => {
            expect(res).to.have.status(400);
            expect(res.text).to.be.equal('Password is required.');
            done();
          });
      });

      it('should not login a user with invalid credentials.', (done) => {
        request(app)
          .post('/api/users/login')
          .send(testUserWithInvalidCredentials)
          .then((res) => {
            expect(res).to.have.status(400);
            expect(res.text).to.be.equal('No user found.');
            done();
          });
      });

      it('should not login a user with invalid password.', (done) => {
        request(app)
          .post('/api/users/login')
          .send(testUserWithInvalidPassword)
          .then((res) => {
            expect(res).to.have.status(400);
            expect(res.text).to.be.equal('Invalid credentials.');
            done();
          });
      });
    });
  });
});
