const expect = require('chai').expect;
const app = mutil.getApp(),
  noApp = '';
const UserRouter = require(__root + 'user/UserRouter');

describe('UserRouter', function () {
  it('should be a function', function () {
    expect(UserRouter).to.be.a('function');
  });
  it('should take one parameter', function () {
    expect(UserRouter.bind(this, app)).to.not.throw(Error);
  });
  it('should throw error if app parameter is missing', function () {
    try {
      UserRouter(noApp);
    } catch (err) {
      expect(err.toString()).to.eql(
        'Error: Missing parameter: "app" not provided.'
      );
    }
  });
  it('should return an express router function', function () {
    expect(UserRouter(app)).to.be.a('function');
  });
});
