/* eslint-disable no-console */
// eslint-disable-next-line node/no-unpublished-require
const MongoMemoryServe = require('mongodb-memory-server');
module.exports = function (app) {
  const mongoose = require('mongoose');
  let reconnect = 0;

  if (app.config.env === 'test') {
    const mongoServer = new MongoMemoryServe.MongoMemoryServer();

    mongoose.Promise = global.Promise;
    mongoServer.getConnectionString().then((mongoUri) => {
      const mongooseOpts = {
        // options for mongoose 4.11.3 and above
        autoReconnect: true,
        reconnectTries: Number.MAX_VALUE,
        reconnectInterval: 1000,
        useMongoClient: true, // remove this line if you use mongoose 5 and above
      };

      mongoose.connect(mongoUri, mongooseOpts);

      mongoose.connection.on('error', (e) => {
        if (e.message.code === 'ETIMEDOUT') {
          console.log(e);
          mongoose.connect(mongoUri, mongooseOpts);
        }
        console.log(e);
      });

      mongoose.connection.once('open', () => {
        console.log(`MongoDB successfully connected to ${mongoUri}`);
      });
    });
  } else {
    /**
     * need to plug in native promises to work with promises instead of callbacks
     */
    mongoose.Promise = global.Promise;

    const connectWithRetry = () => {
      mongoose.connect(app.config.db, { useMongoClient: true }, function (err) {
        if (err) {
          reconnect++;
          if (reconnect === 3) {
            reconnect = 0;
            console.error('Failed to connect to mongodb ', err);
          } else {
            setTimeout(connectWithRetry, 1000);
          }
        }
      });
    };
    connectWithRetry();
  }
};
