function signToken(id, type, app) {
  let jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

  let config = {};
  config.tokenSecret = app.tokenSecret;
  config.expiredIn = app.tokenExpires;

  return jwt.sign(
    {
      id: id,
    },
    config.tokenSecret,
    {
      expiresIn: config.expiredIn,
    }
  );
}

module.exports = {
  signToken: signToken,
};
