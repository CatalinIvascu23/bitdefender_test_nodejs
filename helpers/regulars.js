module.exports = {
  password: /^.{6,100}$/,
  userName: /^\D{2,}/,
};
