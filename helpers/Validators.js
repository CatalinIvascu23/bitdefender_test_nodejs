const regulars = require('./regulars');

function validatePassword(password) {
  return regulars.password.test(password);
}

function validateUserName(userName) {
  if (typeof userName === 'string') {
    return regulars.userName.test(userName.trim());
  } else {
    return false;
  }
}

module.exports = {
  validatePassword: validatePassword,
  validateUserName: validateUserName,
};
