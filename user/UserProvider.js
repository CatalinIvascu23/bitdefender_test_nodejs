/* eslint-disable no-console */
const User = require('./User.js');
const bcrypt = require('bcryptjs-then');
const validator = require('../helpers/Validators');
const signToken = require('../helpers/SignToken').signToken;

module.exports = function (app) {
  return {
    register,
    login,
    logout,
  };

  function hashGenerate(password) {
    return bcrypt.hash(password, 8);
  }

  function register(req, res, next) {
    const { userName, password, confirmPassword } = req.body;

    if (!userName) {
      return res.status(400).send('User name is required.');
    }

    if (!(userName && validator.validateUserName(userName))) {
      return res.status(400).send('Invalid user name.');
    }

    if (!password) {
      return res.status(400).send('Password is required.');
    }

    if (!confirmPassword) {
      return res.status(400).send('Password is required.');
    }

    if (!(password && validator.validatePassword(password))) {
      return res.status(400).send('Invalid password.');
    }

    if (password !== confirmPassword) {
      return res.status(400).send('Password and confirm password must match.');
    }

    return User.findOne({ userName })
      .then((user) => {
        if (user) {
          return res.status(400).send('User already exists.');
        } else {
          hashGenerate(password).then((hash) => {
            const registerUser = {
              userName: userName.trim(),
              password: hash,
            };

            User.create(registerUser)
              .then(() => {
                res.status(201).send('User created');
              })
              .catch((err) => {
                return next(err);
              });
          });
        }
      })
      .catch((err) => next(err));
  }

  function login(req, res, next) {
    let _token;
    const { userName, password } = req.body;

    if (!userName) {
      return res.status(400).send('Username is required.');
    }

    if (!password) {
      return res.status(400).send('Password is required.');
    }

    return User.findOne({ userName })
      .then((user) => {
        const _user = user;
        if (!_user) {
          return res.status(400).send('No user found.');
        }

        bcrypt.compare(password, user.password).then(function (resp) {
          if (resp) {
            _token = signToken(_user._id, 'token', app.config);

            _user.tokens = _token;

            User.findByIdAndUpdate(_user._id, _user, { new: true })
              .then(() => {
                res.setHeader('x-access-token', _token);
                return res.status(200).send('Login was successful!');
              })
              .catch((err) => next(err));
          } else {
            return res.status(400).send('Invalid credentials.');
          }
        });
      })
      .catch((err) => next(err));
  }

  function logout(req, res, next) {
    const token = req.headers['x-access-token'];
    const { id } = req.body;
    let query = { _id: id, tokens: token };

    return User.findOne(query)
      .then((user) => {
        if (user) {
          user.tokens = '';
          User.findByIdAndUpdate(id, user, { new: true })
            .then((updatedUser) => {
              if (updatedUser) {
                res.status(200).send('Logout was successful!');
              }
            })
            .catch((err) => next(err));
        } else {
          return res.status(400).send('No user found.');
        }
      })
      .catch((err) => next(err));
  }
};
