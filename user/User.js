const mongoose = require('mongoose');
const UserSchema = new mongoose.Schema({
  userName: {
    type: String,
    trim: true,
    index: {
      unique: true,
    },
  },
  password: String,
  tokens: String,
});
const User = mongoose.model('User', UserSchema);
module.exports = User;
