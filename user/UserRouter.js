module.exports = function (app) {
  if (!app) throw new Error('Missing parameter: "app" not provided.');

  const express = require('express'),
    UserRouter = express.Router(),
    UserProvider = require('./UserProvider')(app),
    VerifyToken = require(__root + 'helpers/VerifyToken')(app);

  // REGISTER A USER
  UserRouter.post('/register', UserProvider.register);

  // LOGIN A USER
  UserRouter.post('/login', UserProvider.login);

  // LOGOUT A USER
  UserRouter.post('/logout', VerifyToken, UserProvider.logout);

  return UserRouter;
};
