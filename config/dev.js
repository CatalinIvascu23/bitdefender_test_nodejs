const tokenSecret = process.env.SECRET || 'supersecret';
const env = 'dev';

module.exports = {
  tokenSecret: tokenSecret,
  tokenExpires: 5000,
  env: env,
  db: 'mongodb://localhost:27017/node-server-dev',
  maxConnectionAttempts: 10,
  reconnectAfter: 5000,
};
