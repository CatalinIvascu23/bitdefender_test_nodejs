/* eslint-disable no-console */
require('dotenv').config({ path: './config.js' });
const server = require('./app');

server
  .serve(process.env.NODE_ENV)
  .then((app) => {
    console.log('info', `Server running on port ${app.port}`);
  })
  .catch((err) => console.log('error', `Error in app.js at: ${err}`));
