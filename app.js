const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');

global.__root = __dirname + '/';
global.HttpError = require('./helpers/HttpError');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static('lib/files'));

app.use(
  cors({
    origin: 'http://localhost:3000',
    credentials: true,
  })
);

app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

  // Request methods you wish to allow

  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, OPTIONS, PUT, PATCH, DELETE'
  );

  // Request headers you wish to allow
  res.setHeader(
    'Access-Control-Allow-Headers',
    'X-Requested-With,content-type,x-access-token'
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  res.setHeader('Access-Control-Expose-Headers', 'x-access-token');

  // Pass to next layer of middleware
  next();
});

module.exports = {
  serve(env) {
    app.config = require('./config')(env);
    require('./db')(app);
    require('./routes')(app);
    app.port = process.env.PORT || 3127;

    // Liveness probe to be used by Kube API to determine server status
    app.get('/api/health', (req, res) => {
      res.status(200).send('Health check passed');
    });

    return new Promise((resolve, reject) => {
      app.listen(app.port, function (err) {
        if (err) return reject(err);
        resolve(app);
      });
    });
  },
};
