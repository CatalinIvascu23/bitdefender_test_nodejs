module.exports = function (app) {
  const express = require('express');
  const router = express.Router();
  /**
   * routes
   */

  const UserRouter = require('./user/UserRouter')(app);
  router.use('/users', UserRouter);

  const EventRouter = require('./events/EventRouter')(app);
  router.use('/events', EventRouter);

  /**
   * default error handling
   */
  function errorHandler(err, req, res, next) {
    res.status(err.status || 500).send(err.message);
    next();
  }

  router.use(errorHandler);

  /**
   * default root route for api
   */
  app.use('/api', router);
  app.get('/api', (req, res) => res.status(200).send('Api Works.'));
};
